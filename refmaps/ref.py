from turtle import *
import math
from time import sleep
# import PyPDF2
import re
import os
import fitz
from PIL import Image
import math

def page_setup():
    bgcolor("light grey")
    screen = Screen()
    screen.setup(0.95, 0.95, 0, 0)
    area_to_use = (window_width() * 0.8, window_height() * 0.8)

    return area_to_use

def turtle_drawing(area_to_use, linked_connections, linked, label_page, labelled_only, reference_page, referenced_only, sheets, pdf_width, pdf_height, temp_folder, pdf_link):
    tony = Turtle() 
    tony.color('black', 'white')
    tony.penup()
    tony.speed(0)

    page_height, page_width, rows, cols = align_sheets(sheets, area_to_use, pdf_width, pdf_height)
    page_ratio = pdf_height / page_height

    spare = (window_width() * 0.9 - (cols * page_width), window_height() * 0.9 - (rows * page_height))
    padding = (spare[0] / (cols + 1), spare[1] / (rows + 1))

    top_left = ( -((padding[0] * (cols - 1) / 2) + page_width * cols / 2), ((padding[1] * ( rows - 1) / 2) + page_height * rows / 2))
    starting = top_left
    tony.goto(top_left)
    tony.hideturtle()

    sheet_count = 0
    page_origin = []

    pdf_refs = fitz.open(pdf_link)
    screen = Screen()
    
    for rows in range(rows):
            top_left = tony.pos()
            for across in range(cols):
                page_origin.append(tony.pos())
                tony.write(sheet_count + 1)
                tony.goto(tony.pos()[0] + page_width/2, tony.pos()[1] - page_height/2)
                page = pdf_refs[sheet_count]
                pix = page.getPixmap()
                pix.writeImage(os.path.join(temp_folder, "temp_%i.gif" %sheet_count))
                im = Image.open(os.path.join(temp_folder, "temp_%i.gif" %sheet_count))
                im = im.resize((math.floor(im.size[0] / page_ratio), math.floor(im.size[1] / page_ratio)), Image.ANTIALIAS)  # decreases width and height of the image
                im.save(os.path.join(temp_folder, "temp_%i.gif" %sheet_count), optimize=True, quality=85)
                screen.register_shape(os.path.join(temp_folder, "temp_%i.gif" %sheet_count))
                tony.shape(os.path.join(temp_folder, "temp_%i.gif" %sheet_count))
                tony.stamp()
                tony.shape('turtle')
                tony.goto(tony.pos()[0] - page_width/2, tony.pos()[1] + page_height/2)
                tony.forward(page_width + padding[0])
                sheet_count += 1
                if sheet_count == sheets:
                    break
            tony.goto(top_left)
            tony.setheading(270)
            tony.forward(page_height + padding[1])
            tony.setheading(0)

    tony.goto(0, 0)

    colors = ['red', 'blue', 'green', 'orange', 'purple', 'dark olive green', 'dark blue', 'dark green', 'black'] * 10

    tony.goto((starting[0] - padding[0], starting[1] + padding[1]))
    i = 0
    for phrases in linked:
        tony.color(colors[i])
        tony.write(phrases + '  ', move=True, align='left')
        if tony.pos()[0] > -starting[0]:
            tony.goto((starting[0] - padding[0], tony.pos()[1] - 12))
        i = i + 1

    tony.goto((starting[0] - padding[0], -(starting[1] + padding[1])))
    tony.color('black')
    tony.write('Labelled, not referenced: ', move=True, align='left')
    for phrases in labelled_only:
        tony.color(colors[i])
        tony.write(phrases + '  ', move=True, align='left')
        if tony.pos()[0] > -starting[0]:
            tony.goto((starting[0] - padding[0], tony.pos()[1] - 12))
        i = i + 1

    tony.goto((starting[0] - padding[0], -(starting[1] + padding[1])-24))
    tony.color('black')
    tony.write('Referenced, not labelled: ', move=True, align='left')
    for phrases in referenced_only:
        tony.color(colors[i])
        tony.write(phrases + '  ', move=True, align='left')
        if tony.pos()[0] > -starting[0]:
            tony.goto((starting[0] - padding[0], tony.pos()[1] - 12))
        i = i + 1

    i = 0
    tony.pensize(2)
    
    for groups in linked_connections:
        tony.goto(page_origin[groups[0][0]][0] + (groups[0][1][0]+groups[0][1][2])/(2*page_ratio), page_origin[groups[0][0]][1] - (groups[0][1][1]+groups[0][1][3])/(2*page_ratio))
        tony.color(colors[i], colors[i])
        start_line(5, tony)
        for destinations in groups[1:]:
            coords_list = []
            coords_list.append((page_origin[groups[0][0]][0] + (groups[0][1][0]+groups[0][1][2])/(2*page_ratio), page_origin[groups[0][0]][1] - (groups[0][1][1]+groups[0][1][3])/(2*page_ratio)))
            coords_list.append((page_origin[destinations[0]][0] + (destinations[1][0]+destinations[1][2])/(2*page_ratio), page_origin[destinations[0]][1] - (destinations[1][1]+destinations[1][3])/(2*page_ratio)))
            tony.goto(coords_list[0])
            tony.setheading(tony.towards(coords_list[1]))
            tony.pendown()
            tony.goto(coords_list[1])
            arrow_head(tony)
            tony.penup()
        i = i + 1

    for groups in label_page:
        for group in groups:
            tony.goto(page_origin[group[0]][0] + group[1][0]/page_ratio, page_origin[group[0]][1] - group[1][1]/page_ratio)
            tony.pendown()
            tony.color(colors[i], colors[i])
            tony.goto(page_origin[group[0]][0] + group[1][0]/page_ratio, page_origin[group[0]][1] - group[1][3]/page_ratio)
            tony.goto(page_origin[group[0]][0] + group[1][2]/page_ratio, page_origin[group[0]][1] - group[1][3]/page_ratio)
            tony.goto(page_origin[group[0]][0] + group[1][2]/page_ratio, page_origin[group[0]][1] - group[1][1]/page_ratio)
            tony.goto(page_origin[group[0]][0] + group[1][0]/page_ratio, page_origin[group[0]][1] - group[1][1]/page_ratio)
            tony.penup()
        i = i + 1

    for groups in reference_page:
        for group in groups:
            tony.goto(page_origin[group[0]][0] + (group[1][0]+group[1][2])/(2*page_ratio), page_origin[group[0]][1] - (group[1][1]+group[1][3])/(2*page_ratio) - 20)
            tony.color(colors[i], colors[i])
            tony.write('!', align='center', font=('arial',20))
        i = i + 1
    tony.hideturtle()

    for i in range(len(pdf_refs)):
        os.remove(os.path.join(temp_folder, "temp_%i.gif" %i))
    # ts = getscreen()

    # ts.getcanvas().postscript(file="refmap.eps")
    done()

def align_sheets(sheets, area, pdf_width, pdf_height):
    cols = sheets
    rows = math.ceil(sheets / cols)

    edge_ratio = pdf_height / pdf_width

    page_width = area_to_use[0] / cols
    page_height = area_to_use[1] / rows
    if page_width < page_height / edge_ratio: page_height = page_width * edge_ratio
    else: page_width = page_height / edge_ratio
    optimal = False

    while optimal is False:
        trial_rows = rows + 1
        trial_cols = math.ceil(sheets / trial_rows)
        trial_width = area_to_use[0] / trial_cols
        trial_height = area_to_use[1] / trial_rows
        if trial_width < trial_height / edge_ratio: trial_height = trial_width * edge_ratio
        else: trial_width = trial_height / edge_ratio
        if trial_height >= page_height:
            page_height = trial_height
            page_width = trial_width
            rows = trial_rows
            cols = trial_cols
        else:
            optimal = True
    
    return page_height, page_width, rows, cols
    
def file_processing(pdf_link, tex_list):
    pdf = fitz.open(pdf_link)
    sheets = pdf.pageCount

    pdf_page = pdf[0].getPixmap()
    pdf_width = pdf_page.width
    pdf_height = pdf_page.height

    files_list = tex_list
    references = []

    for files in files_list:
        with open(files,"r") as searchfile:
            for line in searchfile:
                if r'\ref{' in line:
                    occurrences = line.count(r'\ref{')
                    for count in range(occurrences):
                        reference = (line.split(r'\ref{')[occurrences].split(r'}')[0])
                        if reference not in references:
                            references.append(reference)

    labels = []

    for files in files_list:
        with open(files,"r") as searchfile:
            for line in searchfile:
                if r'\label{' in line:
                    labels.append((line.split(r'\label{')[1]).split(r'}')[0])

    labelled_only = []
    referenced_only = []
    linked = []

    for label in labels:
        if label in references: linked.append(label)
        else: labelled_only.append(label)

    for reference in references:
        if reference not in linked: referenced_only.append(reference)

    linked_connections = []
    for search in linked:
        working_row = []
    # Extract text and do the search
        for pno in range(len(pdf)):
            page = pdf[pno]
            dic = (page.getText('dict'))
            for blocks in dic['blocks']:
                if blocks['type'] == 0:
                    for l in blocks['lines']:
                        for s in l['spans']:
                            if search in s['text']:
                                if s['color'] == 16711680:
                                    rectangle = s['bbox']
                                    working_row.append((pno, rectangle))
        for pno in range(len(pdf)):
            page = pdf[pno]
            dic = (page.getText('dict'))
            for blocks in dic['blocks']:
                if blocks['type'] == 0:
                    for l in blocks['lines']:
                        for s in l['spans']:
                            if search in s['text']:
                                if s['color'] == 255:
                                    rectangle = s['bbox']
                                    working_row.append((pno, rectangle))
        linked_connections.append(working_row)

    label_page = []
    for search in labelled_only:
        working_row = []
    # Extract text and do the search
        for pno in range(len(pdf)):
            page = pdf[pno]
            dic = (page.getText('dict'))
            for blocks in dic['blocks']:
                if blocks['type'] == 0:
                    for l in blocks['lines']:
                        for s in l['spans']:
                            if search in s['text']:
                                if s['color'] == 16711680:
                                    rectangle = s['bbox']
                                    working_row.append((pno, rectangle))
        label_page.append(working_row)

    reference_page = []
    for search in referenced_only:
        working_row = []
    # Extract text and do the search
        for pno in range(len(pdf)):
            page = pdf[pno]
            dic = (page.getText('dict'))
            for blocks in dic['blocks']:
                if blocks['type'] == 0:
                    for l in blocks['lines']:
                        for s in l['spans']:
                            if search in s['text']:
                                if s['color'] == 255:
                                    rectangle = s['bbox']
                                    working_row.append((pno, rectangle))
        reference_page.append(working_row)

    return linked_connections, linked, label_page, labelled_only, reference_page, referenced_only, sheets, pdf_width, pdf_height

def arrow_head(tony):
    tony.right(135)
    tony.forward(10)
    tony.penup()
    tony.back(10)
    tony.right(90)
    tony.pendown()
    tony.forward(10)
    tony.penup()
    tony.back(10)
    tony.left(225)

def start_line(radius, tony):
    tony.penup()
    tony.setheading(270)
    tony.forward(radius)
    tony.left(90)
    tony.pendown()
    tony.circle(radius)
    tony.penup()
    tony.left(90)
    tony.forward(radius)

def draw_square(width, tony):
    starting_pos = tony.pos()
    tony.penup()
    tony.setheading(270)
    tony.forward(width/2)
    tony.right(90)
    tony.forward(width/2)
    tony.pendown()
    for sides in range(4):
        tony.forward(width)
        tony.right(90)
    tony.penup()
    tony.goto(starting_pos)

# Add the links to your showkeys edited PDF, tex files and folders here

pdf_link = os.path.expanduser(r"~\Downloads\Transfer (4)\Transfer (4).pdf")
tex_dir = os.path.expanduser(r"~\Downloads\Transfer (4)\chapters")
files_list = ['intro.tex', 'current.tex', 'future.tex', 'appendix.tex']
temp_folder = os.path.expanduser(r"~\Downloads\Transfer (4)")

# Uncomment this and fix the links to run the example included on Git

# pdf_link = os.path.expanduser(r"~\Source\refmap\example\foo.pdf")
# tex_dir = os.path.expanduser(r"~\Source\refmap\example")
# files_list = ['foo.tex']
# temp_folder = os.path.expanduser(r"~\Source\refmap\example")

tex_list = []
for files in files_list:
    tex_list.append(os.path.join(tex_dir,files))

area_to_use = page_setup()

linked_connections, linked, label_page, labelled_only, reference_page, referenced_only, sheets, pdf_width, pdf_height = file_processing(pdf_link, tex_list)

turtle_drawing(area_to_use, linked_connections, linked, label_page, labelled_only, reference_page, referenced_only, sheets, pdf_width, pdf_height, temp_folder, pdf_link)
